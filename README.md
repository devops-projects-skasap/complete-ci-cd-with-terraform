## Complete CI/CD with Terraform

### Technologies Used:

Terraform, Jenkins, Docker, AWS, Git, Java, Maven, Linux, Docker Hub

### Project Description

Integrate provisioning stage into complete CI/CD Pipeline to automate provisioning EC2 server instead of deploying to an existing server:

1. Create SSH Key Pair

2. Install Terraform inside Jenkins server

3. Add Terraform configuration to application’s git repository

4. Adjust Jenkinsfile to add “provision” step to the CI/CD pipeline that provisions EC2 instance So the complete CI/CD project we build has the following configuration:

   * CI step: Build artifact for Java Maven application

   * CI step: Build and push Docker image to Docker Hub

   * CD step: Automatically provision EC2 instance using TF

   * CD step: Deploy new application version on the provisioned EC2 instance with Docker Compose

### Instructions:

#### Step 1: Install and configure Jenkins, Docker engine and Terraform on an AWS EC2 Server:

![image](images/jenkins-1.png)

![image](images/jenkins-2.png)

#### Step 2: Create an SSH key pair (`myapp-key-pair`) on AWS for the EC2 server to be provisioned:

![image](images/aws-key-pair.png)

##### Create `AutomatedEC2DeploymentServer` credential of type `SSH username with private key` in Jenkins (based on the SSH key pair) to enable SSH access to the EC2 server from Jenkins:

![image](images/jenkins-credential-1.png)

#### Step 3: Create credentials of type `secret text` in Jenkins for AWS Access Key ID and Secret Access Key to provision EC2 server using Terraform from Jenkins: 

![image](images/jenkins-credential-2.png)

#### Step 4: Create an Docker Hub repository to store Docker image versions for the Java Maven application:

![image](images/dockerhub-1.png)

##### Create `DockerHubLogin` credential of type `username with password` in Jenkins to enable Jenkins and EC2 server to push and pull Docker images to/from the repository on Docker Hub:
  
![image](images/jenkins-credential-3.png)

#### Step 5: Write a Bash script to be executed on the EC2 server during provision only to install Docker engine and Docker Compose tool on the server:

```
#!/bin/bash

yum update -y && yum install -y amazon-linux-extras install docker
                
systemctl start docker
systemctl enable docker
                
usermod -aG docker ec2-user

# Install Docker Compose
curl -L https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
```

#### Step 6: Compose a Terraform script to provision an EC2 instance on AWS, in addition to configuring a VPC, creating a subnet, establishing an internet gateway, and modifying the default route table and default security group:

```
terraform {
  required_version = ">= 0.12"
  backend "s3" {
    bucket = "myapp-bucket-skasap"
    key = "myapp/state.tfstate"
    region = "eu-west-1"
  }
}

resource "aws_vpc" "myapp-vpc" {
  cidr_block = var.vpc_cidr_block

  tags = {
    Name = "${var.env_prefix}-vpc"
  }
}

resource "aws_subnet" "myapp-subnet-1" {
  vpc_id     = aws_vpc.myapp-vpc.id

  cidr_block = var.subnet_cidr_block

  availability_zone = var.availability_zone

  tags = {
    Name = "${var.env_prefix}-subnet-1"
  }
}

resource "aws_internet_gateway" "myapp-igw" {
  vpc_id     = aws_vpc.myapp-vpc.id

  tags = {
    Name = "${var.env_prefix}-igw"
  }

}

// Modify the main/default route table
resource "aws_default_route_table" "main-rtb" {
  default_route_table_id = aws_vpc.myapp-vpc.default_route_table_id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.myapp-igw.id
  }

  tags = {
    Name = "${var.env_prefix}-main-rtb"
  }
}

// Modify the default secuirty group
resource "aws_default_security_group" "default-sg" {
  vpc_id     = aws_vpc.myapp-vpc.id

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [var.ip_addr, var.jenkins_ip_addr]
  }

  ingress {
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    prefix_list_ids = []
  }
  
  tags = {
    Name = "${var.env_prefix}-default-sg"
  }
}

data "aws_ami" "latest-amazon-linux-2-image" {
  most_recent = true
  owners = ["amazon"]

  filter {
    name = "name"
    values = ["amzn2-ami-kernel-5.10-hvm-*-x86_64-gp2"]
  }

  filter {
    name = "virtualization-type"
    values = ["hvm"]
  }
}

resource "aws_instance" "myapp-server" {
  ami = data.aws_ami.latest-amazon-linux-2-image.id
  instance_type = var.instance_type

  subnet_id = aws_subnet.myapp-subnet-1.id
  vpc_security_group_ids = [aws_default_security_group.default-sg.id]
  availability_zone = var.availability_zone

  associate_public_ip_address = true
  key_name = "myapp-key-pair"

  user_data = file("entry-script.sh")

  tags = {
    Name = "${var.env_prefix}-server"
  }
}
```

#### Step 7: Write a Dockerfile to build a docker image for the Java Maven application:

```
FROM amazoncorretto:8-alpine3.17-jre

EXPOSE 8080

COPY ./target/java-maven-app-*.jar /usr/app/
WORKDIR /usr/app

CMD java -jar java-maven-app-*.jar
```

#### Step 8: Compose a Docker Compose yaml file to deploy the new application version on the provisioned EC2 server:

```
version: '3'
services:
  java-maven-app:
    image: "${IMAGE_NAME}${TAG}"
    ports:
      - 8080:8080
  postgres:
    image: postgres:latest
    ports:
      - 5432:5432
    environment:
      - POSTGRES_PASSWORD=password
```

#### Step 9: Write a Bash script to login to Docker Hub and the execute the Docker Compose file:

```
#!/usr/bin/env bash

export IMAGE_NAME=$1
export TAG=$2

export DOCKER_USER=$3
export DOCKER_PASS=$4

echo $DOCKER_PASS | docker login -u $DOCKER_USER --password-stdin

docker-compose -f docker-compose.yaml up -d

echo "Success!"
```

#### Step 10: Write a Groovy script to be called from within the Jenkinsfile:

```
def testApp() {
    sh 'mvn test'
}

def buildJar() {
    sh 'mvn clean package -Dmaven.test.skip=true'
}

def buildImage() {
    withCredentials([usernamePassword(credentialsId: 'DockerHubLogin', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh "docker build -t ${IMAGE_NAME}${TAG} ."
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh "docker push ${IMAGE_NAME}${TAG}"
    }
}

def provisionEC2() {
    sh 'terraform init'
    sh 'terraform apply --auto-approve'

    EC2_PUBLIC_IP = sh(returnStdout: true, script: 'terraform output myapp-server-public-ip').trim()
}

def deployApp() {
    def EC2Server = "ec2-user@${EC2_PUBLIC_IP}"
    def ShellCMD = "bash ./server-cmd.sh ${IMAGE_NAME} ${TAG} ${DOCKER_CREDS_USR} ${DOCKER_CREDS_PSW}"

    sshagent(['AutomatedEC2DeploymentServer']) {
        sh "scp -o StrictHostKeyChecking=no server-cmd.sh ${EC2Server}:/home/ec2-user"
        sh "scp -o StrictHostKeyChecking=no docker-compose.yaml ${EC2Server}:/home/ec2-user"

        sh "ssh -o StrictHostKeyChecking=no ${EC2Server} ${ShellCMD}"
    }
}

return this
```

#### Step 11: Compose a Jenkinsfile with all the required CI and CD stages:

```
def gv

pipeline {
    agent any
    tools {
        maven 'MAVEN3.9'
    }
        environment {
        IMAGE_NAME = 'skasap/java-maven-app:'
    }
    stages {
        stage('Init') {
            steps {
                script {
                    gv = load 'script.groovy'
                }
            }
        }
        stage('Test') {
            steps { 
                script {
                    echo "Testing in branch ${BRANCH_NAME}.."
                    gv.testApp()
                }
            }
        }
        stage('Increment Version') {
            steps {
                script {
                    echo 'Incrementing App version..'
                    sh 'mvn build-helper:parse-version versions:set \
                    -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
                    versions:commit'
                    def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
                    def version = matcher[0][1]

                    env.TAG = "$version-$BUILD_NUMBER"
                }
            }
        }
        stage('Build Jar') {
            steps {
                script {
                    echo 'Building the application Jar..'
                    gv.buildJar()
                }
            }
        }
        stage('Build Image') {
            steps {
                script {
                    echo "Building the Docker image: ${IMAGE_NAME}${TAG}.."
                    gv.buildImage()
                }
            }
        }
        stage('Provision EC2 Server') {
            environment {
               AWS_ACCESS_KEY_ID = credentials('AWSTFAdminAccessKeyID')
               AWS_SECRET_ACCESS_KEY = credentials('AWSTFAdminSecretAccessKey')
               TF_VAR_env_prefix = 'test'
            }
            steps {
                script {
                    echo 'Provisioning EC2 server using Terraform..'
                    dir('terraform') {
                        gv.provisionEC2()
                    }
                }
            }
        }
        stage('Deploy') {
            environment {
                DOCKER_CREDS = credentials('DockerHubLogin')
            }
            steps {
                script {
                    echo 'Waiting for EC2 Server to initialize..'
                    sleep(time: 90, unit: 'SECONDS')

                    echo 'Deploying the application..'
                    echo "Public IP of the server: ${EC2_PUBLIC_IP}"
                    gv.deployApp()
                }
            }
        }
        stage('Commit Version Update')
        {
            steps {
                script {
                    withCredentials([usernamePassword(credentialsId: 'GitLabLogin', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                        sh 'git config --global user.email "jenkins@example.com"'
                        sh 'git config --global user.name "jenkins"'

                        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/devops-projects-skasap/complete-ci-cd-with-terraform.git"
                        sh 'git add .'
                        sh 'git commit -m "CI: Version Bump"'
                        sh 'git push origin HEAD:main'
                    }
                }
            }
        }
    }
}
```

##### Jenkins CI/CD pipeline operation:

![image](images/jenkins-3.png)

##### Docker image versions in the Docker Hub repository:

![image](images/dockerhub-2.png)

##### Docker containers running on the EC2 server:

![image](images/ec2-server.png)


