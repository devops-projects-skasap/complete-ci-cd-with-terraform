def testApp() {
    sh 'mvn test'
}

def buildJar() {
    sh 'mvn clean package -Dmaven.test.skip=true'
}

def buildImage() {
    withCredentials([usernamePassword(credentialsId: 'DockerHubLogin', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
        sh "docker build -t ${IMAGE_NAME}${TAG} ."
        sh "echo $PASS | docker login -u $USER --password-stdin"
        sh "docker push ${IMAGE_NAME}${TAG}"
    }
}

def provisionEC2() {
    sh 'terraform init'
    sh 'terraform apply --auto-approve'

    EC2_PUBLIC_IP = sh(returnStdout: true, script: 'terraform output myapp-server-public-ip').trim()
}

def deployApp() {
    def EC2Server = "ec2-user@${EC2_PUBLIC_IP}"
    def ShellCMD = "bash ./server-cmd.sh ${IMAGE_NAME} ${TAG} ${DOCKER_CREDS_USR} ${DOCKER_CREDS_PSW}"

    sshagent(['AutomatedEC2DeploymentServer']) {
        sh "scp -o StrictHostKeyChecking=no server-cmd.sh ${EC2Server}:/home/ec2-user"
        sh "scp -o StrictHostKeyChecking=no docker-compose.yaml ${EC2Server}:/home/ec2-user"

        sh "ssh -o StrictHostKeyChecking=no ${EC2Server} ${ShellCMD}"
    }
}

return this
