#!/bin/bash

yum update -y && yum install -y amazon-linux-extras install docker
                
systemctl start docker
systemctl enable docker
                
usermod -aG docker ec2-user

# Install Docker Compose
curl -L https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
