variable "vpc_cidr_block" {
  type        = string
  description = "VPC CIDR Block"
  default = "10.0.0.0/16"
}

variable "subnet_cidr_block" {
  type        = string
  description = "Subnet CIDR Block"
  default = "10.0.10.0/24"
}

variable region {
  type = string
  description = "Region"
  default = "eu-west-1"
}

variable availability_zone {
  type = string
  description = "Availability Zone"
  default = "eu-west-1a"
}

variable env_prefix {
  type        = string
  description = "Environment Prefix"
  default = "dev"
}

variable ip_addr {
  type        = string
  description = "IP Address"
  default = "84.66.253.178/32"
}

variable jenkins_ip_addr {
  type        = string
  description = "Jenkins IP Address"
  default = "44.202.229.62/32"
}

variable instance_type {
  type        = string
  description = "Instance Type"
  default = "t2.micro"
}