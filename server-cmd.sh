#!/usr/bin/env bash

export IMAGE_NAME=$1
export TAG=$2

export DOCKER_USER=$3
export DOCKER_PASS=$4

echo $DOCKER_PASS | docker login -u $DOCKER_USER --password-stdin

docker-compose -f docker-compose.yaml up -d

echo "Success!"