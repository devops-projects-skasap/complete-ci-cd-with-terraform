def gv

pipeline {
    agent any
    tools {
        maven 'MAVEN3.9'
    }
        environment {
        IMAGE_NAME = 'skasap/java-maven-app:'
    }
    stages {
        stage('Init') {
            steps {
                script {
                    gv = load 'script.groovy'
                }
            }
        }
        stage('Test') {
            steps { 
                script {
                    echo "Testing in branch ${BRANCH_NAME}.."
                    gv.testApp()
                }
            }
        }
        stage('Increment Version') {
            steps {
                script {
                    echo 'Incrementing App version..'
                    sh 'mvn build-helper:parse-version versions:set \
                    -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
                    versions:commit'
                    def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
                    def version = matcher[0][1]

                    env.TAG = "$version-$BUILD_NUMBER"
                }
            }
        }
        stage('Build Jar') {
            steps {
                script {
                    echo 'Building the application Jar..'
                    gv.buildJar()
                }
            }
        }
        stage('Build Image') {
            steps {
                script {
                    echo "Building the Docker image: ${IMAGE_NAME}${TAG}.."
                    gv.buildImage()
                }
            }
        }
        stage('Provision EC2 Server') {
            environment {
               AWS_ACCESS_KEY_ID = credentials('AWSTFAdminAccessKeyID')
               AWS_SECRET_ACCESS_KEY = credentials('AWSTFAdminSecretAccessKey')
               TF_VAR_env_prefix = 'test'
            }
            steps {
                script {
                    echo 'Provisioning EC2 server using Terraform..'
                    dir('terraform') {
                        gv.provisionEC2()
                    }
                }
            }
        }
        stage('Deploy') {
            environment {
                DOCKER_CREDS = credentials('DockerHubLogin')
            }
            steps {
                script {
                    echo 'Waiting for EC2 Server to initialize..'
                    sleep(time: 90, unit: 'SECONDS')

                    echo 'Deploying the application..'
                    echo "Public IP of the server: ${EC2_PUBLIC_IP}"
                    gv.deployApp()
                }
            }
        }
        stage('Commit Version Update')
        {
            steps {
                script {
                    withCredentials([usernamePassword(credentialsId: 'GitLabLogin', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                        sh 'git config --global user.email "jenkins@example.com"'
                        sh 'git config --global user.name "jenkins"'

                        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/devops-projects-skasap/complete-ci-cd-with-terraform.git"
                        sh 'git add .'
                        sh 'git commit -m "CI: Version Bump"'
                        sh 'git push origin HEAD:main'
                    }
                }
            }
        }
    }
}